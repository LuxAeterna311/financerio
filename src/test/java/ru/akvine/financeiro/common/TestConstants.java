package ru.akvine.financeiro.common;

public class TestConstants {
    public static final String CLIENT_UUID_2 = "client_uuid_2";
    public static final String CLIENT_UUID_4 = "client_uuid_4";
    public static final String CLIENT_UUID_5 = "client_uuid_5";

    public static final String TEST_PHONE = "9800101111";
    public static final String NEW_TEST_PHONE = "9800101010";
}
