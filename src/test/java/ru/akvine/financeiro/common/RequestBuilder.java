package ru.akvine.financeiro.common;

import ru.akvine.financeiro.rest.dto.client.AddClientRequest;
import ru.akvine.financeiro.rest.dto.client.UpdateClientRequest;

import static ru.akvine.financeiro.common.TestConstants.NEW_TEST_PHONE;
import static ru.akvine.financeiro.common.TestConstants.TEST_PHONE;

public class RequestBuilder {
    public AddClientRequest buildAddClientRequest() {
        return new AddClientRequest()
                .setFirstName("firstName")
                .setSecondName("secondName")
                .setThirdName("thirdName")
                .setEmail("test@email.com")
                .setPhone(TEST_PHONE);
    }

    public UpdateClientRequest buildUpdateClientRequest(String uuid) {
        return new UpdateClientRequest()
                .setUuid(uuid)
                .setFirstName("newFirstName")
                .setSecondName("newSecondName")
                .setThirdName("newThirdName")
                .setEmail("newTest@email.com")
                .setPhone(NEW_TEST_PHONE);
    }
}
