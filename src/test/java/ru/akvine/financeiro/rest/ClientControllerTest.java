package ru.akvine.financeiro.rest;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;
import ru.akvine.financeiro.ApiBaseTest;
import ru.akvine.financeiro.common.RequestBuilder;
import ru.akvine.financeiro.rest.common.CommonErrorCodes;
import ru.akvine.financeiro.rest.dto.client.AddClientRequest;
import ru.akvine.financeiro.rest.dto.client.UpdateClientRequest;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.akvine.financeiro.common.RestMethods.*;
import static ru.akvine.financeiro.common.TestConstants.*;

@Transactional
class ClientControllerTest extends ApiBaseTest {
    private final RequestBuilder requestBuilder = new RequestBuilder();

    @Test
    @DisplayName("Get clients SUCCESS")
    void getClients_success() throws Exception {
        doGet(CLIENT_ENDPOINT, null)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("SUCCESS"))
                .andExpect(jsonPath("$.clients").isNotEmpty());
    }

    @Test
    @DisplayName("Get client by uuid SUCCESS")
    void getClientByUuid_success() throws Exception {
        ;
        doGet(CLIENT_ENDPOINT + "/" + CLIENT_UUID_2, null)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("SUCCESS"))
                .andExpect(jsonPath("$.client.uuid").value(CLIENT_UUID_2))
                .andExpect(jsonPath("$.client.firstName").value("client_first_name_2"))
                .andExpect(jsonPath("$.client.secondName").value("client_second_name_2"))
                .andExpect(jsonPath("$.client.thirdName").value("client_third_name_2"))
                .andExpect(jsonPath("$.client.createdDate").exists())
                .andExpect(jsonPath("$.client.phone").value("client_phone_2"))
                .andExpect(jsonPath("$.client.deleted").value(false));
    }

    @Test
    @DisplayName("Get client FAIL. Can't find by uuid")
    void getClient_fail_CantFindByUuid() throws Exception {
        String notExistsClient = "notExistsClient";
        doGet(CLIENT_ENDPOINT + "/" + notExistsClient, null)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.code").value(CommonErrorCodes.CLIENT_NOT_FOUND))
                .andExpect(jsonPath("$.message").value("Client not found wit uuid=" + notExistsClient));
    }

    @Test
    @DisplayName("Add client SUCCESS")
    void addClient_success() throws Exception {
        AddClientRequest request = requestBuilder.buildAddClientRequest();
        doPost(CLIENT_ENDPOINT, request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("SUCCESS"))
                .andExpect(jsonPath("$.client.uuid").exists())
                .andExpect(jsonPath("$.client.firstName").value("firstName"))
                .andExpect(jsonPath("$.client.secondName").value("secondName"))
                .andExpect(jsonPath("$.client.thirdName").value("thirdName"))
                .andExpect(jsonPath("$.client.phone").value(TEST_PHONE))
                .andExpect(jsonPath("$.client.createdDate").exists())
                .andExpect(jsonPath("$.client.deleted").value(false));
    }

    @Test
    @DisplayName("Add client FAIL. Invalid phone")
    void addClient_fail_InvalidPhone() throws Exception {
        String invalidPhone = "Invalid phone";
        AddClientRequest request = requestBuilder
                .buildAddClientRequest()
                .setPhone(invalidPhone);
        doPost(CLIENT_ENDPOINT, request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.code").value(CommonErrorCodes.Validation.PHONE_INVALID))
                .andExpect(jsonPath("$.message").value("Phone=" + invalidPhone + " is invalid!"));
    }

    @Test
    @DisplayName("Add client FAIL. Invalid email")
    void addClient_fail_InvalidEmail() throws Exception {
        String invalidEmail = "InvalidEmail";
        AddClientRequest request = requestBuilder
                .buildAddClientRequest()
                .setEmail(invalidEmail);
        doPost(CLIENT_ENDPOINT, request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.code").value(CommonErrorCodes.Validation.EMAIL_INVALID))
                .andExpect(jsonPath("$.message").value("Email=" + invalidEmail + " is invalid!"));
    }

    @Test
    @DisplayName("Add client FAIL. Phone already exists")
    void addClient_fail_PhoneAlreadyExists() throws Exception {
        String existsPhone = "client_phone_2";
        AddClientRequest request = requestBuilder
                .buildAddClientRequest()
                .setPhone(existsPhone);
        doPost(CLIENT_ENDPOINT, request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.code").value(CommonErrorCodes.Validation.PHONE_EXISTS))
                .andExpect(jsonPath("$.message").value("Phone=" + existsPhone + " is already exists!"));
    }

    @Test
    @DisplayName("Add client FAIL. Email already exists")
    void addClient_fail_EmailAlreadyExists() throws Exception {
        String existsEmail = "client_email_3";
        AddClientRequest request = requestBuilder
                .buildAddClientRequest()
                .setEmail(existsEmail);
        doPost(CLIENT_ENDPOINT, request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.code").value(CommonErrorCodes.Validation.EMAIL_EXISTS))
                .andExpect(jsonPath("$.message").value("Email=" + existsEmail + " is already exists!"));
    }

    @Test
    @DisplayName("Update client SUCCESS")
    void updateClient_success() throws Exception {
        String uuid = CLIENT_UUID_4;
        UpdateClientRequest request = requestBuilder.buildUpdateClientRequest(uuid);
        doPut(CLIENT_ENDPOINT, request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("SUCCESS"))
                .andExpect(jsonPath("$.client.uuid").value(uuid))
                .andExpect(jsonPath("$.client.firstName").value("newFirstName"))
                .andExpect(jsonPath("$.client.secondName").value("newSecondName"))
                .andExpect(jsonPath("$.client.thirdName").value("newThirdName"))
                .andExpect(jsonPath("$.client.phone").value(NEW_TEST_PHONE))
                .andExpect(jsonPath("$.client.createdDate").exists())
                .andExpect(jsonPath("$.client.deleted").value(false));
    }

    @Test
    @DisplayName("Delete client SUCCESS")
    void deleteClient_success() throws Exception {
        String uuid  = CLIENT_UUID_5;
        doDelete(CLIENT_ENDPOINT + "/" + uuid, null)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("SUCCESS"))
                .andExpect(jsonPath("$.time").exists());
    }

    @Test
    @DisplayName("Delete client FAIL. Can't find by uuid")
    void deleteClient_fail_CantFindByUuid() throws Exception {
        String uuid = "doesNotExist";
        doDelete(CLIENT_ENDPOINT + "/" + uuid, null)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.code").value(CommonErrorCodes.CLIENT_NOT_FOUND))
                .andExpect(jsonPath("$.message").value("Client not found wit uuid=" + uuid));
    }
}