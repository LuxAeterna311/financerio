package ru.akvine.financeiro;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.akvine.financeiro.repository.ClientRepository;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@TestPropertySource(
        properties = {
                "spring.jpa.hibernate.ddl-auto=create-drop"
        }
)

public class DataBaseTest {
    @Autowired
    protected ClientRepository clientRepository;
}
