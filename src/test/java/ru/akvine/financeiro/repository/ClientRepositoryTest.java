package ru.akvine.financeiro.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.akvine.financeiro.DataBaseTest;
import ru.akvine.financeiro.entity.ClientEntity;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class ClientRepositoryTest extends DataBaseTest {
    @Test
    @DisplayName("Should find by uuid")
    void shouldFindByUuid() {
        ClientEntity clientEntityToSave = new ClientEntity()
                .setUuid("client_uuid_1")
                .setFirstName("firstName")
                .setSecondName("secondName")
                .setThirdName("thirdName")
                .setEmail("email@gmail.com")
                .setPhone("9800101111")
                .setDeleted(false)
                .setCreatedDate(new Date());

        clientRepository.save(clientEntityToSave);

        String uuid = "client_uuid_1";
        ClientEntity clientEntity = clientRepository
                .findByUuid(uuid)
                .get();

        assertThat(clientEntity).isNotNull();
        assertThat(clientEntity.getFirstName()).isNotNull();
        assertThat(clientEntity.getSecondName()).isNotNull();
        assertThat(clientEntity.getThirdName()).isNotNull();
    }

    @Test
    @DisplayName("Should find by phone")
    void shouldFindByPhone() {
        ClientEntity clientEntityToSave = new ClientEntity()
                .setUuid("client_uuid_1")
                .setFirstName("firstName")
                .setSecondName("secondName")
                .setThirdName("thirdName")
                .setEmail("email@gmail.com")
                .setPhone("9800101111")
                .setDeleted(false)
                .setCreatedDate(new Date());

        clientRepository.save(clientEntityToSave);

        ClientEntity clientEntity = clientRepository
                .findByPhone("9800101111")
                .get();


        assertThat(clientEntity).isNotNull();
        assertThat(clientEntity.getFirstName()).isNotNull();
        assertThat(clientEntity.getSecondName()).isNotNull();
        assertThat(clientEntity.getThirdName()).isNotNull();
    }

    @Test
    @DisplayName("Should find by email")
    void shouldFindByEmail() {
        ClientEntity clientEntityToSave = new ClientEntity()
                .setUuid("client_uuid_1")
                .setFirstName("firstName")
                .setSecondName("secondName")
                .setThirdName("thirdName")
                .setEmail("email@gmail.com")
                .setPhone("9800101111")
                .setDeleted(false)
                .setCreatedDate(new Date());

        clientRepository.save(clientEntityToSave);

        ClientEntity clientEntity = clientRepository
                .findByEmail("email@gmail.com")
                .get();


        assertThat(clientEntity).isNotNull();
        assertThat(clientEntity.getFirstName()).isNotNull();
        assertThat(clientEntity.getSecondName()).isNotNull();
        assertThat(clientEntity.getThirdName()).isNotNull();
    }
}
