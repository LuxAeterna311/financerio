INSERT INTO CLIENT (ID, UUID, FIRST_NAME, SECOND_NAME, THIRD_NAME, PHONE, EMAIL, UPDATED_DATE, CREATED_DATE, DELETED_DATE, IS_DELETED)
VALUES (1, 'client_uuid_1', 'client_first_name_1', 'client_second_name_1', null, null, null, null, '2022-05-02 09:17:40.431', null, 0);
INSERT INTO CLIENT (ID, UUID, FIRST_NAME, SECOND_NAME, THIRD_NAME, PHONE, EMAIL, UPDATED_DATE, CREATED_DATE, DELETED_DATE, IS_DELETED)
VALUES (2, 'client_uuid_2', 'client_first_name_2', 'client_second_name_2', 'client_third_name_2', 'client_phone_2', null, null, '2022-05-02 09:17:40.431', null, 0);
INSERT INTO CLIENT (ID, UUID, FIRST_NAME, SECOND_NAME, THIRD_NAME, PHONE, EMAIL, UPDATED_DATE, CREATED_DATE, DELETED_DATE, IS_DELETED)
VALUES (3, 'client_uuid_3', 'client_first_name_3', 'client_second_name_3', 'client_third_name_3', 'client_phone_3', 'client_email_3', null, '2022-05-02 09:17:40.431', null, 0);
INSERT INTO CLIENT (ID, UUID, FIRST_NAME, SECOND_NAME, THIRD_NAME, PHONE, EMAIL, UPDATED_DATE, CREATED_DATE, DELETED_DATE, IS_DELETED)
VALUES (4, 'client_uuid_4', 'client_first_name_4', 'client_second_name_4', 'client_third_name_4', 'client_phone_4', 'client_email_4', null, '2022-05-02 09:17:40.431', null, 0);
INSERT INTO CLIENT (ID, UUID, FIRST_NAME, SECOND_NAME, THIRD_NAME, PHONE, EMAIL, UPDATED_DATE, CREATED_DATE, DELETED_DATE, IS_DELETED)
VALUES (5, 'client_uuid_5', 'client_first_name_5', 'client_second_name_5', 'client_third_name_5', 'client_phone_5', 'client_email_5', null, '2022-05-02 09:17:40.431', null, 0);