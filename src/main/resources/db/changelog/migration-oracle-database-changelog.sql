--liquibase formatted sql logicalFilePath:db/changelog/migration-oracle-1.0-initial-schema.sql

--changeset lymar-sa:FIN-3-1
--preconditions onFail:MARK_RAN onError:HALT onUpdateSql:FAIL
--precondition-sql-check expectedResult:0 select count(*) from information_schema.tables where table_name = 'client'
CREATE TABLE CLIENT (
    ID BIGINT NOT NULL,
    UUID VARCHAR (255) NOT NULL,
    FIRST_NAME VARCHAR (255) NOT NULL,
    SECOND_NAME VARCHAR (255) NOT NULL,
    THIRD_NAME VARCHAR (255),
    PHONE VARCHAR(255),
    EMAIL VARCHAR(255),
    CREATED_DATE TIMESTAMP NOT NULL,
    UPDATED_DATE TIMESTAMP,
    DELETED_DATE TIMESTAMP NULL,
    IS_DELETED BOOLEAN NOT NULL DEFAULT FALSE
);
CREATE SEQUENCE CLIENT_ID_SEQUENCE START WITH 1 INCREMENT BY 1000;
--rollback not required