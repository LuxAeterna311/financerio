package ru.akvine.financeiro.util;

import ru.akvine.financeiro.exception.validation.ValidationException;

import java.util.UUID;

public class UuidGeneratorUtil {
    public static String generate() {
        return UUID.randomUUID().toString().toUpperCase();
    }

    public static String generate(int length) {
        String generatedUuid = generate();
        validate(generatedUuid, length);
        return generatedUuid.substring(0, length);
    }

    public static String generate(String target) {
        return generate().replace(target, "");
    }

    public static String generate(int length, String target) {
        String generatedUuid = generate().replace(target, "");
        validate(generatedUuid, length);
        return generatedUuid.substring(0, length);
    }

    public static String generate(int length, String target, String replacement) {
        String generatedUuid = generate().replace(target, replacement);
        validate(generatedUuid, length);
        return generatedUuid.substring(0, length);
    }

    private static void validate(String uuid, int length) {
        if (length > uuid.length() || length < -1) {
            throw new ValidationException("Length can't be less -1 or more than generated uuid");
        }
    }

}
