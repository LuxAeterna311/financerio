package ru.akvine.financeiro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.akvine.financeiro.entity.ClientEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<ClientEntity, Long> {
    @Query("from ClientEntity ce where ce.uuid = :uuid and ce.deleted = false")
    Optional<ClientEntity> findByUuid(@Param("uuid") String uuid);

    @Query("from ClientEntity ce where ce.phone = :phone and ce.deleted = false")
    Optional<ClientEntity> findByPhone(@Param("phone") String phone);

    @Query("from ClientEntity ce where ce.email = :email and ce.deleted = false")
    Optional<ClientEntity> findByEmail(@Param("email") String email);

    @Query("from ClientEntity ce where ce.deleted = false")
    List<ClientEntity> findAll();
}
