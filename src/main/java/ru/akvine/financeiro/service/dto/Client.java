package ru.akvine.financeiro.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.akvine.financeiro.entity.ClientEntity;

import java.util.Date;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
public class Client {
    private String uuid;
    private String firstName;
    private String secondName;
    private String thirdName;
    private String phone;
    private String email;
    private Date updatedDate;
    private Date createdDate;
    private boolean deleted;

    public Client(ClientEntity clientEntity) {
        this.uuid = clientEntity.getUuid();
        this.firstName = clientEntity.getFirstName();
        this.secondName = clientEntity.getSecondName();
        this.thirdName = clientEntity.getThirdName();
        this.phone = clientEntity.getPhone();
        this.email = clientEntity.getEmail();
        this.updatedDate = clientEntity.getUpdatedDate();
        this.createdDate = clientEntity.getCreatedDate();
        this.deleted = clientEntity.isDeleted();
    }
}
