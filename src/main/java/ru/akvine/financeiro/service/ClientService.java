package ru.akvine.financeiro.service;

import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.akvine.financeiro.entity.ClientEntity;
import ru.akvine.financeiro.exception.ClientNotFoundException;
import ru.akvine.financeiro.repository.ClientRepository;
import ru.akvine.financeiro.service.dto.Client;
import ru.akvine.financeiro.util.UuidGeneratorUtil;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ClientService {
    private final ClientRepository clientRepository;

    @Value("${uuid.generator.length}")
    private int uuidGeneratorLength;

    public Client getByUuid(String uuid) {
        Preconditions.checkNotNull(uuid, "uuid is null");
        return new Client(getEntityByUuid(uuid));
    }

    public Client getByPhone(String phone) {
        Preconditions.checkNotNull(phone, "phone is null");
        return new Client();
    }

    public ClientEntity getEntityByUuid(String uuid) {
        Preconditions.checkNotNull(uuid, "uuid is null");
        return clientRepository
                .findByUuid(uuid)
                .orElseThrow(() -> new ClientNotFoundException("Client not found wit uuid=" + uuid));
    }

    public ClientEntity getEntityByPhone(String phone) {
        Preconditions.checkNotNull(phone, "phone is null");
        return clientRepository
                .findByPhone(phone)
                .orElseThrow(() -> new ClientNotFoundException("Client not found with phone=" + phone));
    }

    public List<Client> getList() {
        return clientRepository
                .findAll()
                .stream()
                .map(Client::new)
                .collect(Collectors.toList());
    }

    public Client add(Client client) {
        Preconditions.checkNotNull(client, "client is null");

        ClientEntity clientEntity = new ClientEntity()
                .setUuid(UuidGeneratorUtil.generate(uuidGeneratorLength, "-"))
                .setFirstName(client.getFirstName())
                .setSecondName(client.getSecondName())
                .setThirdName(client.getThirdName())
                .setPhone(client.getPhone())
                .setEmail(client.getEmail())
                .setCreatedDate(new Date());
        return new Client(clientRepository.save(clientEntity));
    }

    @Transactional
    public Client update(Client client) {
        Preconditions.checkNotNull(client, "client is null");
        Preconditions.checkNotNull(client.getUuid(), "client.uuid is null");

        ClientEntity clientEntityToUpdate = getEntityByUuid(client.getUuid())
                .setFirstName(client.getFirstName())
                .setSecondName(client.getSecondName())
                .setThirdName(client.getThirdName())
                .setPhone(client.getPhone())
                .setEmail(client.getEmail())
                .setUpdatedDate(new Date());

        return new Client(clientRepository.save(clientEntityToUpdate));
    }

    @Transactional
    public void deleteByUuid(String uuid) {
        Preconditions.checkNotNull(uuid, "uuid is null");
        ClientEntity clientEntity = getEntityByUuid(uuid);
        clientEntity.setDeleted(true);
        clientEntity.setDeletedDate(new Date());
        clientRepository.save(clientEntity);
    }

    public boolean isExistsByPhone(String phone) {
        Preconditions.checkNotNull(phone, "phone is null");
        return clientRepository
                .findByPhone(phone)
                .isPresent();
    }

    public boolean isExistsByEmail(String email) {
        Preconditions.checkNotNull(email, "email is null");
        return clientRepository
                .findByEmail(email)
                .isPresent();
    }
}
