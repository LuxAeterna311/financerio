package ru.akvine.financeiro.rest;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.akvine.financeiro.converter.ClientConverter;
import ru.akvine.financeiro.rest.common.SuccessfulResponse;
import ru.akvine.financeiro.rest.dto.client.AddClientRequest;
import ru.akvine.financeiro.rest.dto.client.UpdateClientRequest;
import ru.akvine.financeiro.rest.meta.ClientControllerMeta;
import ru.akvine.financeiro.rest.validator.ClientValidator;
import ru.akvine.financeiro.service.ClientService;
import ru.akvine.financeiro.service.dto.Client;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Tag(name = "Client API")
public class ClientController implements ClientControllerMeta {
    private final ClientConverter clientConverter;
    private final ClientService clientService;
    private final ClientValidator clientValidator;

    @Override
    public SuccessfulResponse getByUuid(String uuid) {
        Client client = clientService.getByUuid(uuid);
        return clientConverter.convertToClientResponse(client);
    }

    @Override
    public SuccessfulResponse getClients() {
        List<Client> clients = clientService.getList();
        return clientConverter.convertToClientListResponse(clients);
    }

    @Override
    public SuccessfulResponse addClient(@RequestBody AddClientRequest request) {
        clientValidator.verifyAddClientRequest(request);
        Client client = clientConverter.convertToClient(request);
        Client savedClient = clientService.add(client);
        return clientConverter.convertToClientResponse(savedClient);
    }

    @Override
    public SuccessfulResponse updateClient(@RequestBody UpdateClientRequest request) {
        clientValidator.verifyUpdateClientRequest(request);
        Client client = clientConverter.convertToClient(request);
        Client updatedClient = clientService.update(client);
        return clientConverter.convertToClientResponse(updatedClient);
    }

    @Override
    public SuccessfulResponse deleteByUuid(@PathVariable("uuid") String uuid) {
        clientService.deleteByUuid(uuid);
        return new SuccessfulResponse();
    }
}
