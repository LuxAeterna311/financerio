package ru.akvine.financeiro.rest;

import org.springframework.web.bind.annotation.RestController;
import ru.akvine.financeiro.rest.dto.health.HealthResponse;
import ru.akvine.financeiro.rest.dto.health.HealthStatus;
import ru.akvine.financeiro.rest.meta.HealthControllerMeta;

@RestController
public class HealthController implements HealthControllerMeta {
    @Override
    public HealthResponse checkHealth() {
        return new HealthResponse()
                .setState(HealthStatus.UP);
    }
}
