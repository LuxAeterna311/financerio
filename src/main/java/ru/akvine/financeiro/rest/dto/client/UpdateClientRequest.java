package ru.akvine.financeiro.rest.dto.client;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class UpdateClientRequest {
    private String uuid;
    private String firstName;
    private String secondName;
    private String thirdName;
    private String phone;
    private String email;
}
