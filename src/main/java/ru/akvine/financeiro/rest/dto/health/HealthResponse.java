package ru.akvine.financeiro.rest.dto.health;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain = true)
public class HealthResponse {
    private HealthStatus state;
}
