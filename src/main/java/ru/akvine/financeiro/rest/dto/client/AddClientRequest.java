package ru.akvine.financeiro.rest.dto.client;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain = true)
public class AddClientRequest {
    private String firstName;
    private String secondName;
    private String thirdName;
    private String phone;
    private String email;
}
