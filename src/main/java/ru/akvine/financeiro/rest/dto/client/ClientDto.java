package ru.akvine.financeiro.rest.dto.client;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.akvine.financeiro.service.dto.Client;

import java.util.Date;

@Getter
@Setter
@Accessors(chain = true)
public class ClientDto {
    private String uuid;
    private String firstName;
    private String secondName;
    private String thirdName;
    private String phone;
    private String email;
    private Date createdDate;
    private Date updatedDate;
    private boolean deleted;

    public ClientDto(Client client) {
        this.uuid = client.getUuid();
        this.firstName = client.getFirstName();
        this.secondName = client.getSecondName();
        this.thirdName = client.getThirdName();
        this.phone = client.getPhone();
        this.email = client.getEmail();
        this.createdDate = client.getCreatedDate();
        this.updatedDate = client.getUpdatedDate();
        this.deleted = client.isDeleted();
    }
}
