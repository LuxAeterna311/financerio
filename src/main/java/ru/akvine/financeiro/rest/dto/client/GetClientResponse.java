package ru.akvine.financeiro.rest.dto.client;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.akvine.financeiro.rest.common.SuccessfulResponse;

@Getter
@Setter
@Accessors(chain = true)
public class GetClientResponse extends SuccessfulResponse {
    private ClientDto client;
}
