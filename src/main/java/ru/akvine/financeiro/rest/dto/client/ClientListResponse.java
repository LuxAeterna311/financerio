package ru.akvine.financeiro.rest.dto.client;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.akvine.financeiro.rest.common.SuccessfulResponse;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class ClientListResponse extends SuccessfulResponse {
    private List<ClientDto> clients;
}
