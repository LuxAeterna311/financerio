package ru.akvine.financeiro.rest.validator;

import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.akvine.financeiro.exception.validation.EmailExistsException;
import ru.akvine.financeiro.exception.validation.PhoneExistsException;
import ru.akvine.financeiro.rest.dto.client.AddClientRequest;
import ru.akvine.financeiro.rest.dto.client.UpdateClientRequest;
import ru.akvine.financeiro.service.ClientService;
import ru.akvine.financeiro.validator.EmailValidator;
import ru.akvine.financeiro.validator.PhoneValidator;

@Component
@RequiredArgsConstructor
public class ClientValidator {
    private final PhoneValidator phoneValidator;
    private final EmailValidator emailValidator;
    private final ClientService clientService;

    public void verifyAddClientRequest(AddClientRequest request) {
        Preconditions.checkNotNull(request, "addClientRequest is null");
        String phone = request.getPhone();
        String email = request.getEmail();
        verify(phone, email);
    }

    public void verifyUpdateClientRequest(UpdateClientRequest request) {
        Preconditions.checkNotNull(request, "updateClientRequest is null");
        String phone = request.getPhone();
        String email = request.getEmail();
        verify(phone, email);
    }

    private void verify(String phone, String email) {
        if (clientService.isExistsByPhone(phone)) {
            throw new PhoneExistsException("Phone=" + phone + " is already exists!");
        }
        if (clientService.isExistsByEmail(email)) {
            throw new EmailExistsException("Email=" + email + " is already exists!");
        }

        phoneValidator.validate(phone);
        emailValidator.validate(email);
    }
}
