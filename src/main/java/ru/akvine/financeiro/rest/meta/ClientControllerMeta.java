package ru.akvine.financeiro.rest.meta;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.*;
import ru.akvine.financeiro.rest.common.ErrorResponse;
import ru.akvine.financeiro.rest.common.SuccessfulResponse;
import ru.akvine.financeiro.rest.dto.client.AddClientRequest;
import ru.akvine.financeiro.rest.dto.client.ClientListResponse;
import ru.akvine.financeiro.rest.dto.client.ClientResponse;
import ru.akvine.financeiro.rest.dto.client.UpdateClientRequest;

@RequestMapping("/api/v1/clients")
public interface ClientControllerMeta {
    @Operation(summary = "Get client by uuid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Got client",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ClientResponse.class))),
            @ApiResponse(responseCode = "400",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "Can't find endpoint",
                    content = @Content)
    })
    @GetMapping("/{uuid}")
    SuccessfulResponse getByUuid(@PathVariable("uuid") String uuid);

    @Operation(summary = "Get clients")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Got clients",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ClientListResponse.class))),
            @ApiResponse(responseCode = "400",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "Can't find endpoint",
                    content = @Content)
    })
    @GetMapping
    SuccessfulResponse getClients();

    @Operation(summary = "Create client")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Save client",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ClientResponse.class))),
            @ApiResponse(responseCode = "400",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "Can't find endpoint",
                    content = @Content)
    })
    @PostMapping
    SuccessfulResponse addClient(@RequestBody AddClientRequest request);

    @Operation(summary = "Update client")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated client",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ClientResponse.class))),
            @ApiResponse(responseCode = "400",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "Can't find endpoint",
                    content = @Content)
    })
    @PutMapping
    SuccessfulResponse updateClient(@RequestBody UpdateClientRequest request);

    @Operation(summary = "Delete client")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful response",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = SuccessfulResponse.class))),
            @ApiResponse(responseCode = "400",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "Can't find endpoint",
                    content = @Content)
    })
    @DeleteMapping("/{uuid}")
    SuccessfulResponse deleteByUuid(@PathVariable("uuid") String uuid);
}
