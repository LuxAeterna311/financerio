package ru.akvine.financeiro.rest.meta;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.akvine.financeiro.rest.dto.health.HealthResponse;

@RequestMapping("/health")
public interface HealthControllerMeta {
    @GetMapping("/check")
    HealthResponse checkHealth();
}
