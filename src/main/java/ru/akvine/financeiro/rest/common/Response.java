package ru.akvine.financeiro.rest.common;

public interface Response {
    ResponseStatus getStatus();
}
