package ru.akvine.financeiro.rest.common;

public class CommonErrorCodes {
    public static final String CLIENT_NOT_FOUND = "client.notFound.error";

    public interface Validation {
        String VALIDATION_GENERAL = "validation.error";
        String PHONE_INVALID = "phone.invalid.error";
        String EMAIL_INVALID = "email.invalid.error";
        String PHONE_EXISTS = "phone.exists.error";
        String EMAIL_EXISTS = "email.exists.error";
    }
}
