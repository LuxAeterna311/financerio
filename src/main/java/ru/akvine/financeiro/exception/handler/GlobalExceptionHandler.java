package ru.akvine.financeiro.exception.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.akvine.financeiro.exception.ClientNotFoundException;
import ru.akvine.financeiro.exception.validation.*;
import ru.akvine.financeiro.rest.common.CommonErrorCodes;
import ru.akvine.financeiro.rest.common.ErrorResponse;

import java.util.Date;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    @ExceptionHandler({ClientNotFoundException.class})
    public ResponseEntity<ErrorResponse> handleClientNotFoundException(ClientNotFoundException exception) {
        logger.error("Client not found error", exception);
        ErrorResponse errorResponse = new ErrorResponse(
                CommonErrorCodes.CLIENT_NOT_FOUND,
                exception.getMessage(),
                new Date());
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ValidationException.class})
    public ResponseEntity<ErrorResponse> handleValidationException(ValidationException exception) {
        logger.error("Validation error", exception);
        ErrorResponse errorResponse = new ErrorResponse(
                CommonErrorCodes.Validation.VALIDATION_GENERAL,
                exception.getMessage(),
                new Date()
        );
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({PhoneInvalidException.class})
    public ResponseEntity<ErrorResponse> handlePhoneInvalidException(PhoneInvalidException exception) {
        logger.error("Phone invalid error", exception);
        ErrorResponse errorResponse = new ErrorResponse(
                CommonErrorCodes.Validation.PHONE_INVALID,
                exception.getMessage(),
                new Date()
        );
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({EmailInvalidException.class})
    public ResponseEntity<ErrorResponse> handleEmailInvalidException(EmailInvalidException exception) {
        logger.error("Email invalid error", exception);
        ErrorResponse errorResponse = new ErrorResponse(
                CommonErrorCodes.Validation.EMAIL_INVALID,
                exception.getMessage(),
                new Date()
        );
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({PhoneExistsException.class})
    public ResponseEntity<ErrorResponse> handlePhoneExistsException(PhoneExistsException exception) {
        logger.error("Phone exists error", exception);
        ErrorResponse errorResponse = new ErrorResponse(
                CommonErrorCodes.Validation.PHONE_EXISTS,
                exception.getMessage(),
                new Date()
        );
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({EmailExistsException.class})
    public ResponseEntity<ErrorResponse> handleEmailExistsException(EmailExistsException exception) {
        logger.error("Email exists error", exception);
        ErrorResponse errorResponse = new ErrorResponse(
                CommonErrorCodes.Validation.EMAIL_EXISTS,
                exception.getMessage(),
                new Date()
        );
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
}
