package ru.akvine.financeiro.exception.validation;

public class PhoneInvalidException extends RuntimeException {
    public PhoneInvalidException(String message) {
        super(message);
    }
}
