package ru.akvine.financeiro.exception.validation;

public class PhoneExistsException extends RuntimeException {
    public PhoneExistsException(String message) {
        super(message);
    }
}
