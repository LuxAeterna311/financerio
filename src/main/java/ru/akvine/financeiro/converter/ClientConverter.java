package ru.akvine.financeiro.converter;

import com.google.common.base.Preconditions;
import org.springframework.stereotype.Component;
import ru.akvine.financeiro.rest.dto.client.*;
import ru.akvine.financeiro.service.dto.Client;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ClientConverter {
    public ClientResponse convertToClientResponse(Client client) {
        Preconditions.checkNotNull(client, "client is null");
        return new ClientResponse()
                .setClient(new ClientDto(client));
    }

    public ClientListResponse convertToClientListResponse(List<Client> clients) {
        Preconditions.checkNotNull(clients, "clients is null");

        List<ClientDto> clientDtoList = clients
                .stream()
                .map(ClientDto::new)
                .collect(Collectors.toList());

        return new ClientListResponse()
                .setClients(clientDtoList);
    }

    public Client convertToClient(AddClientRequest request) {
        Preconditions.checkNotNull(request, "addClientRequest is null");
        return new Client()
                .setFirstName(request.getFirstName())
                .setSecondName(request.getSecondName())
                .setThirdName(request.getThirdName())
                .setPhone(request.getPhone())
                .setEmail(request.getEmail());
    }

    public Client convertToClient(UpdateClientRequest request) {
        Preconditions.checkNotNull(request, "updateClientRequest is null");
        return new Client()
                .setUuid(request.getUuid())
                .setFirstName(request.getFirstName())
                .setSecondName(request.getSecondName())
                .setThirdName(request.getThirdName())
                .setPhone(request.getPhone())
                .setEmail(request.getEmail());
    }
}
