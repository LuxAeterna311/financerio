package ru.akvine.financeiro.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import ru.akvine.financeiro.exception.validation.PhoneInvalidException;

import java.util.regex.Pattern;

@Component
public class PhoneValidator implements Validator<String> {
    private final String REGEX_PATTERN = "^\\d{10}$";

    @Override
    public void validate(String phone) {
        if (StringUtils.isBlank(phone) || !Pattern.matches(REGEX_PATTERN, phone)) {
            throw new PhoneInvalidException("Phone=" + phone + " is invalid!");
        }
    }
}
