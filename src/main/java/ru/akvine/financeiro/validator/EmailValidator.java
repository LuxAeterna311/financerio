package ru.akvine.financeiro.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import ru.akvine.financeiro.exception.validation.EmailInvalidException;

import java.util.regex.Pattern;

@Component
public class EmailValidator implements Validator<String> {
    private final static String REGEX_PATTERN = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";

    @Override
    public void validate(String email) {
        if (StringUtils.isBlank(email) || !Pattern.matches(REGEX_PATTERN, email)) {
            throw new EmailInvalidException("Email=" + email + " is invalid!");
        }
    }
}
