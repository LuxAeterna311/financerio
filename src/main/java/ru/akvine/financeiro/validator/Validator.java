package ru.akvine.financeiro.validator;

public interface Validator<T> {
    void validate(T obj);
}
